﻿using Microsoft.AspNetCore.Identity;

namespace AutenticationAPI.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
