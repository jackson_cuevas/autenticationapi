﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutenticationAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;

namespace AutenticationAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ValuesController : ControllerBase
    {
        private readonly IDataProtector _protector;
        private readonly HashService _hashService;
        public ValuesController(IDataProtectionProvider protectionProvider, HashService hashService)
        {
            _protector = protectionProvider.CreateProtector("valor_unico_y_quizas_secroto");
            _hashService = hashService;
        }

        //PAGINACION
        // GET api/values
        //[HttpGet]
        //[EnableCors("PermitirRequest")]
        //public Task<ActionResult<IEnumerable<string>>> Get(int numeroDePagina = 1, int cantidadDeRegistros)
        //{
        //    var query = ContextBoundObject.Autores.AsQueryable()

        //    var totalDeRegistros = query.Count();

        //    var autores = await query
        //        .Skip(cantidadDeRegistros * (numeroDePagina - 1))
        //        .Take(cantidadDeRegistros)
        //        .ToListAsync();

        //    Response.Headers["X-Total-Registros"] = totalDeRegistros.ToString();
        //    Response.Headers["X-Cantidad-Paginas"] = 
        //        ((int))Math.Ceiling((double))totalDeRegistros / cantidadDeRegistros)).ToString();

        //    var autoresDTO = mapper.Map<List<AutorDTO>>(autores);
        //    return autoresDTO;
        //}

        [HttpGet("hash")]
        public ActionResult GetHash()
        {
            string textoPlano = "Jackson Cuevas";
            var hashResult1 = _hashService.Hash(textoPlano).Hash;
            var hashResult2 = _hashService.Hash(textoPlano).Hash;
            return Ok( new { textoPlano, hashResult1, hashResult2});
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            string textoPlano = "Jackson Cuevas";
            string textoCifrado = _protector.Protect(textoPlano);
            string textoDesencriptado = _protector.Unprotect(textoCifrado);

            return Ok(new { textoPlano, textoCifrado, textoDesencriptado });
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
